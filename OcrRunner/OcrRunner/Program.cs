﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace OcrRunner
{
    class Program
    {

        internal static cSQL o_cSqlChimp = new cSQL();
        internal static cSQL o_cSqlChimpCmd = new cSQL();
        public static string gs_LogFullFileNamePath = @"e:\apps\ocrrunner\ocrrunnerLog.txt";
        static void Main(string[] args)
        {
#pragma warning disable CS0618 // Type or member is obsolete
            string connStr = ConfigurationSettings.AppSettings["chimpconnstr"];
#pragma warning restore CS0618 // Type or member is obsolete

            o_cSqlChimp.ConnectionString = connStr;
            o_cSqlChimpCmd.ConnectionString = connStr;
            WriteToLog(DateTime.Now.ToString() + "\tStarting Chimp OcrRunner......");
            //Program.SendEmailAlert("Starting :: ", "ChimpOcrRunner ");

            //if (CheckStatus("HYPERV"))
            //    Run();

            Test1();
            Console.WriteLine("{0} : Operations completed", DateTime.Now);
            WriteToLog(DateTime.Now.ToString() + " : Operations completed");
            //Console.ReadLine();
            Environment.Exit(0);
        }

        public static bool CheckStatus(string machine)
        {
            try
            {
                string s_sql = @"Select isactive from jobconfig WHERE service like 'OCR' and machine like '" + machine + "';";
                var retVal = o_cSqlChimpCmd.RunSQLReturnNumeric(s_sql);
                return Convert.ToBoolean(retVal);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Getting Job Config: " + ex.ToString());
                WriteToLog(DateTime.Now.ToString() + " Error Getting Job Config: " + ex.ToString());
            }
            return false;
        }
        public static void Test1()
        {
            string ocrText = @"Vel	93.3 cm/s
PG	3 mmHg
MV Peak A
Vel
Vel	89.8 cm/s
PG	3 mmHg
TDI
Med E' Vel 6.34 cm/s
Comments
NO CHNGS SINCE LAST CHO
TRACE MR
MILD LVH
WALLACH, DENNIS
06/11/2018
";
            string res = ocrText;
            //res = res.Replace("\n", "\r\n");
            res = res.Replace("\n", Environment.NewLine);
            //remove all quotes
            res = res.Replace("\"", string.Empty).Replace("'", string.Empty);
            DataSet replacements = new DataSet();
            replacements = o_cSqlChimp.RunSQLReturnDataSet("SELECT src, dest FROM ocrreplacements where id = 58 order by LENGTH(src) DESC");
            if (replacements.Tables[0].Rows.Count > 0)
                foreach (DataRow row in replacements.Tables[0].Rows)
                {
                    res = res.Replace(row["src"].ToString(), row["dest"].ToString());
                }
            byte[] bytes = Encoding.ASCII.GetBytes(res);
            char[] chars = Encoding.ASCII.GetChars(bytes);
            string line = new String(chars);
            line = line.Replace("?", "");
            res = line;

        }
        public static void Test()
        {
            //string tesseractlocation = @"e:/apps/tesseract4/tesseract.exe";
            string imagemagicklocation = @"c:/program files/ImageMagick-7.0.7-Q16/convert.exe";
            string result;



            System.Drawing.Image img = System.Drawing.Image.FromFile(@" e:\apps\Imagick\60.jpg");
            //MessageBox.Show("Width: " + img.Width + ", Height: " + img.Height);



            using (Process p = new Process())
            {
                string filename = @"Z:\output\1.2.276.0.7230010.3.1.4.233392925.508.1505938998.1\test.jpg";

                string ocrexepath = imagemagicklocation.Replace(@"/", @"\");
                string allArguments = @" e:\apps\Imagick\60.jpg -crop 1325x1500+10+500 e:\apps\Imagick\6cropprog.png";
                p.StartInfo.FileName = ocrexepath;
                p.StartInfo.Arguments = allArguments;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.Start();
                result = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                p.Close();
            }

            }
        public static void Run()
        {
            DataRow ocrRec;
            int hitCtr = 0;

            using (Process p = new Process())
            {
                string filename = @"e:\apps\ocrrunner\reconnect.bat";

                string ocrexepath = filename.Replace(@"/", @"\");
                string allArguments = "";
                p.StartInfo.FileName = ocrexepath;
                p.StartInfo.Arguments = allArguments;
                p.StartInfo.UseShellExecute = false;
                p.Start();
                p.WaitForExit();
                p.Close();
            }



            while (true)
            {
                ocrRec = GetOcrRecord();
                if (ocrRec != null)
                    Process(ocrRec);
                else
                {
                    hitCtr += 1;
                    Console.WriteLine("Waiting");
                    System.Threading.Thread.Sleep(60000);
                    if (hitCtr == 10)
                    {
                        WriteToLog(DateTime.Now.ToString() + "\tExiting from no activity Chimp OCRRunner......");
                        return;
                    }
                }
            }
        }

        public static DataRow GetOcrRecord()
        {

            try
            {
                string s_sql = @"SELECT ocrqueue.id, ocrqueue.study_id,ocrqueue.ocrconfig_id,ocrqueue.study_path,ocrconfig.ocr_options,core_study.study_id as outputpath,core_study.manufacturer FROM ocrqueue 
INNER JOIN core_study on core_study.id = ocrqueue.study_id 
INNER join ocrconfig on ocrconfig.id = ocrqueue.ocrconfig_id 
where ocrqueue.status = 1 and core_study.complete = 1 order by ocrqueue.id limit 1";
                DataSet ds = new DataSet();
                ds = o_cSqlChimp.RunSQLReturnDataSet(s_sql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        return row;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error getting OcrQueue Record: " + ex.ToString());
                WriteToLog(DateTime.Now.ToString() + " Error getting OcrQueue Record: " + ex.ToString());
            }
            return null;
        }
        public static void Process(DataRow ocrRec)
        {
            Console.WriteLine(ocrRec["study_path"].ToString());  
            try
            {
                string s_sql = @"UPDATE ocrqueue SET status = 2 WHERE id = " + ocrRec[0].ToString();
                o_cSqlChimpCmd.RunSQL(s_sql);

                if (ocrRec["manufacturer"].ToString() == "MINDRAY-M7")
                    RunZoneOcr(ocrRec);
                else
                    RunOcr(ocrRec);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Processing Movie Record: " + ex.ToString());
                WriteToLog(DateTime.Now.ToString() + " Error Processing Movie Record: " + ex.ToString());
            }
        }
        public static void RunZoneOcr(DataRow ocrRec)
        {
            string cmdlineoptions = ocrRec["ocr_options"].ToString();   //ocr_options

            try
            {
                string ocrTextOutputFolder = @"e:/apps/ocrrunner/ocrTextOutput/";
                //string serverOutputFolder = movieRec[6].ToString().Replace(@"/home/chimp/app/chimp/../output/", @"Z:/output/");
                string serverOutputFolder = @"z:\output\";
                string localOutputFolder = @"e:\apps\Imagick\";
                string tesseractlocation = @"e:/apps/tesseract4/tesseract.exe";
                string croppedImage = @"croppedImage.png";

                string s_sql = String.Format("Select * from core_image where study_id = {0} and result_candidate = 1 order by series_number", ocrRec["study_id"].ToString());
                DataSet ds = new DataSet();
                ds = o_cSqlChimp.RunSQLReturnDataSet(s_sql);
                if (ds.Tables[0].Rows.Count > 0)
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        //insert ocrfiles here
                        s_sql = String.Format(@"insert into ocrfiles(DateCreated,study_id,ocrconfig_id,ocrqueue_id,study_path,core_image_id,study_name,status)
values(now(),{0},{1},{2},'{3}',{4},'{5}',1)",
ocrRec["study_id"].ToString(), ocrRec["ocrconfig_id"].ToString(), ocrRec["id"].ToString(), ocrRec["outputpath"].ToString(), row["id"].ToString(), row["out_name"].ToString());

                        o_cSqlChimpCmd.RunSQL(s_sql);
                    }

                //now process off of ocrfiles records
                string result = string.Empty;
                string fullResult = string.Empty;

                s_sql = String.Format("Select * from ocrfiles where study_id = {0} and status = 1 order by id", ocrRec["study_id"].ToString());
                DataSet dsfiles = new DataSet();
                dsfiles = o_cSqlChimp.RunSQLReturnDataSet(s_sql);
                if (dsfiles.Tables[0].Rows.Count > 0)
                    foreach (DataRow row in dsfiles.Tables[0].Rows)
                    {
                        //Create temp zone file here and pass it to RunOcrCommand
                        CreateZoneOcrFile(serverOutputFolder + row["study_path"].ToString() + "/", row["study_name"].ToString());

                        result = RunOcrCommand(localOutputFolder , croppedImage, ocrTextOutputFolder + ocrRec["study_id"].ToString() + "_" + row["id"].ToString(), tesseractlocation, ocrRec["ocr_options"].ToString());
                        fullResult = fullResult + result;
                        //update ocrfiles here
                        s_sql = String.Format(@"update ocrfiles set status = {0},raw_ocr_result = '{1}' where id = {2}", 2, result, row["id"].ToString());
                        o_cSqlChimpCmd.RunSQL(s_sql);
                    }
                s_sql = String.Format(@"Update core_study set raw_ocr_result='{0}',ocr=1 where id = {1}", fullResult, ocrRec["study_id"].ToString());
                o_cSqlChimpCmd.RunSQL(s_sql);
            }

            catch (Exception ex)
            {
                Console.WriteLine("Error Running ZoneOcr: " + ex.ToString());
                WriteToLog(DateTime.Now.ToString() + " Error Running ZoneOcr: " + ex.ToString());
            }

        }

        public static void CreateZoneOcrFile(string imageoutputfolder, string imagename)
        {

            System.Drawing.Image img = System.Drawing.Image.FromFile(imageoutputfolder + imagename);

            int status = 0;
            string croppedImage = @"e:/apps/Imagick/croppedImage.png";
            string cropValues = "-crop 1325x1500+10+500";
            if(img.Width == 1280)
                cropValues = "-crop 552x625+4+208";

            string allArguments = imageoutputfolder + imagename + " " + cropValues + " " + croppedImage;
            string result;

            string imageMagickLocation = @"c:/program files/ImageMagick-7.0.7-Q16/convert.exe";

            using (Process p = new Process())
            {
                string ocrexepath = imageMagickLocation.Replace(@"/", @"\");
                p.StartInfo.FileName = ocrexepath;
                p.StartInfo.Arguments = allArguments;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.Start();
                result = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                p.Close();
            }
        }
        public static void RunOcr(DataRow ocrRec)
        {
            string cmdlineoptions = ocrRec["ocr_options"].ToString();   //ocr_options

            try
            {
                string ocrTextOutputFolder = @"e:/apps/ocrrunner/ocrTextOutput/";
                //string serverOutputFolder = movieRec[6].ToString().Replace(@"/home/chimp/app/chimp/../output/", @"Z:/output/");
                string serverOutputFolder = @"z:\output\";
                string tesseractlocation = @"e:/apps/tesseract4/tesseract.exe";

                string s_sql = String.Format("Select * from core_image where study_id = {0} and result_candidate = 1 order by series_number", ocrRec["study_id"].ToString());
                DataSet ds = new DataSet();
                ds = o_cSqlChimp.RunSQLReturnDataSet(s_sql);
                if (ds.Tables[0].Rows.Count > 0)
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        //insert ocrfiles here
                        s_sql = String.Format(@"insert into ocrfiles(DateCreated,study_id,ocrconfig_id,ocrqueue_id,study_path,core_image_id,study_name,status)
values(now(),{0},{1},{2},'{3}',{4},'{5}',1)",
ocrRec["study_id"].ToString(), ocrRec["ocrconfig_id"].ToString(), ocrRec["id"].ToString(), ocrRec["outputpath"].ToString(), row["id"].ToString(), row["out_name"].ToString());

                        o_cSqlChimpCmd.RunSQL(s_sql);
                    }

                //now process off of ocrfiles records
                string result = string.Empty;
                string fullResult = string.Empty;

                s_sql = String.Format("Select * from ocrfiles where study_id = {0} and status = 1 order by id", ocrRec["study_id"].ToString());
                DataSet dsfiles = new DataSet();
                dsfiles = o_cSqlChimp.RunSQLReturnDataSet(s_sql);
                if (dsfiles.Tables[0].Rows.Count > 0)
                    foreach (DataRow row in dsfiles.Tables[0].Rows)
                    {

                        result = RunOcrCommand(serverOutputFolder + row["study_path"].ToString() + "/", row["study_name"].ToString(), ocrTextOutputFolder + ocrRec["study_id"].ToString() + "_" + row["id"].ToString(), tesseractlocation, ocrRec["ocr_options"].ToString());
                        fullResult = fullResult + result;
                        //update ocrfiles here
                        s_sql = String.Format(@"update ocrfiles set status = {0},raw_ocr_result = '{1}' where id = {2}",2, result,row["id"].ToString());
                        o_cSqlChimpCmd.RunSQL(s_sql);
                    }
                s_sql = String.Format(@"Update core_study set raw_ocr_result='{0}',ocr=1 where id = {1}", fullResult, ocrRec["study_id"].ToString());
                o_cSqlChimpCmd.RunSQL(s_sql);
            }

            catch (Exception ex)
            {
                Console.WriteLine("Error Running Ocr: " + ex.ToString());
                WriteToLog(DateTime.Now.ToString() + " Error Running Ocr: " + ex.ToString());
            }


        }
        public static string RunOcrCommand(string imageoutputfolder, string imagename, string ocroutputname, string tesseractlocation, string ocroptions)
        {
            int status = 0;

            string allArguments = imageoutputfolder + imagename + " " + ocroutputname + " " + ocroptions;
            string result;

            using (Process p = new Process())
            {
                string ocrexepath = tesseractlocation.Replace(@"/", @"\");
                allArguments = allArguments.Replace(@"/", @"\");
                p.StartInfo.FileName = ocrexepath;
                p.StartInfo.Arguments = allArguments;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.Start();
                result = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                p.Close();

                string ocrpath = ocroutputname + ".txt";
                ocrpath = ocrpath.Replace(@"/", @"\");
                string res = File.ReadAllText(ocrpath);
                result = DoReplacements(res);
                File.Delete(ocrpath);
            }

            return result;
        }

        public static string DoReplacements(string ocrText)
        {
            string res = ocrText;
            //res = res.Replace("\n", "\r\n");
            res = res.Replace("\n", Environment.NewLine);
            //remove all quotes
            res = res.Replace("\"", string.Empty).Replace("'", string.Empty);
            //res = res.Replace("L¥OT Ciam", "LVOT DIAM");
            //res = res.Replace("IySd", "IVSD");
            //res = res.Replace("LyPWWd", "LVPWd");
            //res = res.Replace("ESY(Teich)", "ESV(Teich)");
            //res = res.Replace("L¥OT", "LVOT");
            //res = res.Replace("LYOT", "LVOT");
            //res = res.Replace("¢mis", "cm/s");
            //res = res.Replace("cmis", "cm/s");
            //res = res.Replace("cmIs", "cm/s");
            //res = res.Replace("emis", "cm/s");

            DataSet replacements = new DataSet();
            replacements = o_cSqlChimp.RunSQLReturnDataSet("SELECT src, dest FROM ocrreplacements order by LENGTH(src) DESC");
            if (replacements.Tables[0].Rows.Count > 0)
                foreach (DataRow row in replacements.Tables[0].Rows)
                {
                    res = res.Replace(row["src"].ToString(), row["dest"].ToString());
                }
            byte[] bytes = Encoding.ASCII.GetBytes(res);
            char[] chars = Encoding.ASCII.GetChars(bytes);
            string line = new String(chars);
            line = line.Replace("?", "");
            res = line;
            return res;
        }
        public static bool SendEmailAlert(string s_Body, string s_Subj = "", bool IsBodyHtml = false, bool IsFailure = false)
        {
            //disable actual email send for testing
            //return true;


            try
            {
                MailMessage mail = new MailMessage();

                //set the addresses
                mail.From = new MailAddress("chimpalerter@gmail.com", "Chimp OcrRunner Alerts");

                if (IsFailure == true)
                {
                    //foreach (string email in Program.o_Params.ary_EmailListFailure)
                    //{
                    //    mail.To.Add(email);
                    //}
                    mail.To.Add("steveiz@yahoo.com");
                    //mail.To.Add("alex@sediagnostics.com");
                }
                else
                {
                    //foreach (string email in Program.o_Params.ary_EmailList)
                    //{
                    //    mail.To.Add(email);
                    //}
                    mail.To.Add("steveiz@yahoo.com");
                    //mail.To.Add("alex@ysediagnostics.com");
                }

                //set the content
                mail.Subject = s_Subj + "  - " + DateTime.Now;
                mail.Body = s_Body;
                mail.IsBodyHtml = IsBodyHtml;

                //send the message
                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                smtp.EnableSsl = true;

                //to authenticate we set the username and password properites on the SmtpClient
                smtp.Credentials = new NetworkCredential("chimpalerter@gmail.com", "Unforgiven10!");

                smtp.Send(mail);

                return true;

            }
            catch (Exception ex)
            {
                WriteToLog("Error SendEmailAlert: " + ex.Message);
                return false;

            }
        }
        public static void WriteToLog(string s_Message, string s_Break = "None", bool b_CRLF = false)
        {

            FileStream fs = default(FileStream);
            StreamWriter sw = default(StreamWriter);

            try
            {
                fs = new FileStream(gs_LogFullFileNamePath, FileMode.Append);
                sw = new StreamWriter(fs);

                if (b_CRLF)
                    sw.WriteLine();

                if (s_Break != "Bottom" & s_Break != "None")
                {
                    //Write Top Break
                    sw.WriteLine("************************************************************************");
                }

                sw.WriteLine(s_Message);

                if (s_Break != "Top" & s_Break != "None")
                {
                    //Write Bottom Break
                    sw.WriteLine("************************************************************************");
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Error Writing to log: " + ex.ToString());
                WriteToLog(DateTime.Now.ToString() + " Error Writing to log: " + ex.ToString());
            }
            finally
            {
                if ((sw != null))
                {
                    sw.Flush();
                    sw.Close();
                }

                if ((fs != null))
                {
                    fs.Close();
                }
            }
        }
    }
}
