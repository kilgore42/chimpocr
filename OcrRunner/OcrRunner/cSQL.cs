﻿/********************************************************************************************************************************
* SQL Functions
*
*   Author: S. Thomas
*   Created: 02/2011
*   Notes:
*   
* 
*  Revision History:
*
********************************************************************************************************************************/
using System;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
//using System.Data.SqlClient;
using System.Text;

namespace OcrRunner
{
    public class cSQL
    {
        internal MySqlConnection mo_conn;

        private string str_ConnectionString;

        public string ConnectionString
        {


            get { return str_ConnectionString; }


            set { str_ConnectionString = value; }
        }


        public bool RunSQL(string s_sql)
        {

            MySqlConnection o_conn = GetConnection();
            MySqlCommand o_command = new MySqlCommand(s_sql, o_conn);

            try
            {
                o_command.CommandTimeout = 0;
                //Program.WriteToLog("RunSQL: " + s_sql);
                o_command.ExecuteNonQuery();
                //o_conn.Close()
                return true;
            }
            catch (Exception ex)
            {
                //o_conn.Close()
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: ex= " + ex.ToString());
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: s_sql= " + s_sql);
                FatalError("RunSQL", ex.ToString(), "cSQL", s_sql, false);
                return false;
            }
        }



        //runs an sql statement and returns the last id of a
        //record inserted.

        public int RunSQLReturnID(string s_sql)
        {



            MySqlConnection o_conn = GetConnection();

            try
            {
                //o_conn.Open();
                s_sql += " SELECT @@IDENTITY";
                MySqlCommand o_command = new MySqlCommand(s_sql, o_conn);

                object result = o_command.ExecuteScalar();

                if (result != null && result != DBNull.Value)
                {
                    return Convert.ToInt32(result);
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                //o_conn.Close()
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: ex= " + ex.ToString());
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: s_sql= " + s_sql);
                FatalError("RunSQLReturnID", ex.ToString(), "cSQL", s_sql, false);
                return 0;
            }

        }

        //returns a string from one field after sql statement 
        //is ran.

        public string RunSQLReturnString(string s_sql)
        {

            MySqlConnection o_conn = GetConnection();
            MySqlCommand o_command = new MySqlCommand(s_sql, o_conn);

            try
            {
                object result = o_command.ExecuteScalar();

                if (result != null && result != DBNull.Value)
                {
                    return Convert.ToString(result);
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                //o_conn.Close()
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: ex= " + ex.ToString());
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: s_sql= " + s_sql);
                FatalError("RunSQLReturnID", ex.ToString(), "cSQL", s_sql, false);
                return string.Empty;
            }

        }

        public int RunSQLReturnNumeric(string s_sql)
        {

            MySqlConnection o_conn = GetConnection();

            MySqlCommand o_command = new MySqlCommand(s_sql, o_conn);

            try
            {
                object result = o_command.ExecuteScalar();

                if (result != null && result != DBNull.Value)
                {
                    return Convert.ToInt32(result);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                //o_conn.Close()
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: ex= " + ex.ToString());
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: s_sql= " + s_sql);
                FatalError("RunSQLReturnNumeric", ex.ToString(), "cSQL", s_sql, false);
                return 0;
            }

        }

        public MySqlDataReader RunSQLReturnReader(string s_sql)
        {

            try
            {
                MySqlCommand o_command = new MySqlCommand();

                MySqlConnection o_conn = GetConnection();

                {
                    o_command.CommandType = CommandType.Text;
                    o_command.Connection = o_conn;
                    o_command.CommandText = s_sql;
                    o_command.CommandTimeout = 0;
                }


                return o_command.ExecuteReader(CommandBehavior.Default);


            }
            catch (Exception ex)
            {
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: ex= " + ex.ToString());
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: s_sql= " + s_sql);
                FatalError("RunSQLReturnReader", ex.ToString(), "cSQL", s_sql, false);
            }

            return null;

        }

        public DataSet RunSQLReturnDataSet(string s_sql)
        {

            try
            {
                MySqlConnection o_conn = GetConnection();
                MySqlDataAdapter o_command = new MySqlDataAdapter(s_sql, o_conn);
                DataSet o_ds = new DataSet();

                o_command.SelectCommand.CommandType = CommandType.Text;
                o_command.SelectCommand.CommandTimeout = 0;

                o_command.Fill(o_ds);

                return o_ds;

            }
            catch (Exception ex)
            {
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: ex= " + ex.ToString());
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: s_sql= " + s_sql);
                FatalError("RunSQLReturnDataSet", ex.ToString(), "cSQL", s_sql, false);
            }

            return null;

        }

        public cSQL()
            : base()
        {
        }


        public DataRow RunSQLReturnDataRow(string s_sql)
        {

            try
            {
                MySqlConnection o_conn = GetConnection();
                MySqlDataAdapter o_command = new MySqlDataAdapter(s_sql, o_conn);
                DataSet o_ds = new DataSet();

                o_command.SelectCommand.CommandType = CommandType.Text;
                o_command.Fill(o_ds);

                if (o_ds.Tables[0].Rows.Count > 0)
                {
                    return o_ds.Tables[0].Rows[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: ex= " + ex.ToString());
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: s_sql= " + s_sql);
                FatalError("RunSQLReturnDataRow", ex.ToString(), "cSQL", s_sql, false);
            }
            return null;
        }


        public MySqlConnection GetConnection()
        {

            try
            {
                if ((mo_conn == null))
                {
                    mo_conn = new MySqlConnection(str_ConnectionString);
                    mo_conn.Open();
                    return mo_conn;
                }
                else
                {
                    return mo_conn;
                }

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                switch (ex.Number)
                {

                    case 2:
                        //Sql Service is not available or Credentials are incorrect

                        FatalError("GetConnection", ex.ToString(), "cSQL", "", true);

                        break;
                    case 53:
                        //Server does not exist

                        FatalError("GetConnection", ex.ToString(), "cSQL", "", true);

                        break;
                    default:

                        FatalError("GetConnection", ex.ToString(), "cSQL", "", true);

                        break;
                }




            }
            return null;
        }

        #region " Custom Functions"

        public string EncodeText(string s_text)
        {

            try
            {
                StringBuilder o_string = new StringBuilder();
                string s_temp = null;
                string s_sQ = "" + (char)39;

                o_string.Append(s_text);
                s_temp = o_string.Replace(s_sQ, s_sQ + s_sQ).ToString();
                o_string = null;

                return s_temp;
            }
            catch (Exception ex)
            {
                Program.WriteToLog(DateTime.Now.ToString() + "Calling Fatal Error: ex= " + ex.ToString());
                FatalError("EncodeText", ex.ToString(), "cSQL", "", false);

            }
            return null;
        }


        public void FatalError(string s_procedure, string s_error, string s_modulename, string s_additionalInfo = "", bool bl_redirect = false)
        {
            string s_sql = null;



            try
            {

                Program.WriteToLog(DateTime.Now.ToString() + "     cSQL FATAL Error: Exiting Program");
                Program.SendEmailAlert(s_error + " :: " + s_procedure, "ChimpMovieMaker cSQL FATAL Error: Exiting Program");
                Console.WriteLine("Fatal Error Exiting");
                System.Environment.Exit(1);


            }
            catch (Exception ex)
            {
                //Sql Server Not Responding
                Console.WriteLine("cSQL Error: " + ex.ToString());
                Program.WriteToLog(DateTime.Now.ToString() + "     cSQL Statement Causing Error: " + s_sql);
                Program.WriteToLog(DateTime.Now.ToString() + "     cSQL Error: " + ex.ToString());
                Program.WriteToLog(DateTime.Now.ToString() + "     cSQL FATAL Error: Exiting Program");
                Program.SendEmailAlert(s_error + " :: " + s_procedure, "ChimpMovieMaker cSQL FATAL Error: Exiting Program");
                Console.WriteLine("Fatal Error Exiting");
                System.Environment.Exit(1);

            }



        }

        public static string EncodeForStoredProcedure(string s_text)
        {

            StringBuilder o_string = new StringBuilder();
            string s_temp = null;
            string s_sQ = "" + (char)39;

            o_string.Append(s_text);
            s_temp = o_string.Replace(s_sQ, s_sQ + s_sQ + s_sQ + s_sQ).ToString();
            o_string = null;
            return s_temp;

        }

        public static string SQLEncodeText(string s_text)
        {

            string s_sQ = "" + (char)39;
            StringBuilder o_string = new StringBuilder();
            string s_temp = null;

            o_string.Append(s_text);
            s_temp = o_string.Replace(s_sQ, s_sQ + s_sQ).ToString();
            o_string = null;
            return s_temp;

        }

        #endregion


    }

}
